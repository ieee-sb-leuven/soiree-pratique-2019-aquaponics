import network
import ntptime 
import _thread
from time import sleep
import utime
from machine import Timer, Pin, RTC, PWM, ADC
import onewire
import ds18b20
import usocket
import json
tim = Timer(-1)

#code for the motor
class Motor:
    def __init__(self,pwm):
        self.pwm = pwm
        self.pwm.freq(500)
        self.pwm.duty(0)
        self.dutycycle = 0
        self.onvalue = 30

    def on(self):
        self.dutycycle = self.onvalue
        self.pwm.duty(self.dutycycle)

    def off(self):
        self.dutycycle = 0
        self.pwm.duty(0)

    def duty(self, value = -1):
        if value == -1:
            return self.onvalue
        self.onvalue = value
        if self.dutycycle > 0:
            self.on()

    def value(self):
        return self.dutycycle

# load saved schedule
try:
    from scheduleconfig import schedule
except:
    schedule = {"ledsonhour":[],"ledsonmin":[],"ledsoffhour":[],"ledsoffmin":[],"motoronhour":[],"motoronmin":[],"motoroffhour":[],"motoroffmin":[]}

# load minimal dirt value
try:
    from dirtconfig import mindirtval
except:
    mindirtval=100

try:
    from motorconfig import motorontime,motorofftime
except:
    motorontime=10
    motorofftime = 60
movingAverage=0

# prepare default HTTP answers
FAIL_HEADER = """\
HTTP/1.0 404 NOT FOUND
Connection: close
Server: Brecht
Access-Control-Allow-Origin : *
Content-Type: text/html

"""
HTTP_HEADER = """\
HTTP/1.0 200 OK
Connection: close
Server: Brecht
Access-Control-Allow-Origin : *
Content-Type: text/html

"""

# Configure Wifi
wlan = network.WLAN(network.STA_IF)
wlan.active(True)

# configure real time clock
rtc = RTC()

# connect to network
if not wlan.isconnected():
    print('connecting to network...')
    wlan.connect('YOURSSID', 'YOURPASSWORD')
    while not wlan.isconnected():
        print('.')
        sleep(0.2)
    print('network config:', wlan.ifconfig())

# load pins pins
try:
    from pins import *
except:
    ledpin = 18
    motorpin = 4
    irpin = 5
    irsenspin = 36
    temperaturepin = 32

# configure pins
leds = Pin(ledpin,Pin.OUT)
motor = Motor(PWM(Pin(motorpin)))
ir = Pin(irpin,Pin.OUT)
irsens = ADC(Pin(irsenspin,Pin.IN))
temperature = ds18b20.DS18X20(onewire.OneWire(Pin(temperaturepin)))

# turn off and initialize
leds.off()
irsens.atten(ADC.ATTN_2_5DB)
ir.off()
temperature_rom = temperature.scan()


# save time value
def set_time():
    t = ntptime.time()
    tm = utime.localtime(t)
    tm = tm[0:3] + (0,) + tm[3:6] + (0,)
    h = list(tm)
    h[4] += 2
    rtc.datetime(tuple(h))

# turn switch on or off
# syntax: turn(leds,"on")
def turn(switch, status="on"):
    if (status is "on"):
	    switch.on()
    else:
        switch.off()

# read temperature
def read_temperature():
	temperature.convert_temp()
	temp = temperature.read_temp(temperature_rom[0])
	return temp

# read dirtyness of the water
def read_dirt():
    temp = motor.value()
    motorduty = motor.duty()
    motor.duty(511)
    turn(motor,"on")
    sleep(1)
    turn(motor,"off")
    oldstate = leds.value()
    turn(leds,"off")
    ir.off()
    sleep(0.1)
    ground = 0
    for i in range(5):
        ground = ground + irsens.read()
    ir.on()
    sleep(0.1)
    value = 0
    for i in range(5):
        value = value + irsens.read()
    ir.off()
    motor.duty(motorduty)
    if temp > 0:
        turn(motor,"on")
    print(value)
    print(ground)
    leds.value(oldstate)
    dirtval = 100-(value-ground)/200
    global mindirtval
    print(mindirtval)
    if dirtval < mindirtval:
        mindirtval=dirtval
        with open('dirtvalue.py','rw') as dirtfile:
              dirtfile.write('mindirtval = ' + str(mindirtval));
              dirtfile.close()
    dirtval = (dirtval - mindirtval)
    global movingAverage
    movingAverage = movingAverage*.9 + dirtval*.1
    return movingAverage*20

# start http ser
def http_thread():
    s = usocket.socket(usocket.AF_INET, usocket.SOCK_STREAM)
    s.bind(('',8080))
    s.listen(-1)

    while True:
        res = s.accept()
        _thread.start_new_thread(answer_http,(res))

def answer_http(socket,addr):
    client_s = socket
    client_addr = addr
    client_s.settimeout(1)
    print("Client address:", client_addr)
    print("Client socket:", client_s)
    print("Request:")
    try:
        req = client_s.recv(1024)
        print(req)
        parts = req.decode('ascii').split(' ')
        if len(parts) < 2:
          client_s.close()
          return 0
        elif parts[0] == 'POST':
            if parts[1] == '/postSchedule.php':
                jsonstringIndex = parts[len(parts)-1].find('{')
                print(parts[len(parts)-1][jsonstringIndex:])
                jsonstring = parts[len(parts)-1][jsonstringIndex:]
                with open('scheduleconfig.py','rw') as schedulefile:
                  schedulefile.write('schedule = ' + jsonstring);
                  schedulefile.close()
                global schedule
                schedule = json.loads(jsonstring)
                client_s.send(bytes(HTTP_HEADER,"ascii"))
            elif parts[1] == '/postTiming.php':
                jsonstringIndex = parts[len(parts)-1].find('{')
                print(parts[len(parts)-1][jsonstringIndex:])
                jsonstring = parts[len(parts)-1][jsonstringIndex:]
                motortimings = json.loads(jsonstring)
                global motorontime, motorofftime
                motorontime = int(motortimings["motorontime"])
                motorofftime = int(motortimings["motorofftime"])
                with open('motorconfig.py','rw') as motorfile:
                  motorfile.write('motorontime = ' + str(motorontime))
                  motorfile.write('motorofftime = ' + str(motorofftime))
                  motorfile.close()
                client_s.send(bytes(HTTP_HEADER,"ascii"))
        elif parts[1] == '/motoon':
          client_s.send(bytes(HTTP_HEADER,"ascii"))
          _thread.start_new_thread(pump_thread, ())
          client_s.send(bytes(str(motor.value()),"ascii"))
          client_s.sendall('\n')
        elif parts[1] == '/ledson':
          client_s.send(bytes(HTTP_HEADER,"ascii"))
          turn(leds,"on")
          _thread.start_new_thread(led_thread, ())
          client_s.send(bytes(str(leds.value()),"ascii"))
          client_s.sendall('\n')
        elif parts[1] == '/ledsoff':
          client_s.send(bytes(HTTP_HEADER,"ascii"))
          turn(leds,"off")
          _thread.start_new_thread(led_thread, ())
          client_s.send(bytes(str(leds.value()),"ascii"))
          client_s.sendall('\n')
        elif parts[1] == '/temperature':
          client_s.send(bytes(HTTP_HEADER,"ascii"))
          temp = read_temperature()
          client_s.send(bytes(str(temp),"ascii"))
          client_s.sendall('\n')
        elif parts[1] == '/dirt':
          client_s.send(bytes(HTTP_HEADER,"ascii"))
          temp = read_dirt()
          client_s.send(bytes(str(temp),"ascii"))
          client_s.sendall('\n')
        elif parts[1] == '/':
          client_s.send(bytes(HTTP_HEADER,"ascii"))
          with open('index.html','r') as html:
            client_s.send(html.read())
        elif parts[1].find('/SetMoto') >= 0:
          client_s.send(bytes(HTTP_HEADER,"ascii"))
          val = parts[1].find('=')
          print(int(parts[1][val+1:]))
          motor.duty(int(parts[1][val+1:]))
          client_s.send(bytes(str(motor.duty()),"ascii"))
        elif parts[1] == '/schedule':
          client_s.send(bytes(HTTP_HEADER,"ascii"))
          global schedule
          client_s.send(bytes(str(json.dumps(schedule)),"ascii"))
        elif parts[1] == '/motortimings':
          client_s.send(bytes(HTTP_HEADER,"ascii"))
          global motorontime,motorofftime
          motortimings = { "motorontime":motorontime,"motorofftime":motorofftime}
          client_s.send(bytes(str(json.dumps(motortimings)),"ascii"))
        else:
          client_s.send(bytes(FAIL_HEADER,"ascii"))
        print('closing the connection')
        client_s.close()
    except OSError:
        client_s.close()
        print("timeout")

# configure time
set_time()

# start http server
_thread.start_new_thread(http_thread, ())

# led to obtain manual control
ledslock = _thread.allocate_lock()
def led_thread():
    global ledslock
    testlock = ledslock.acquire(0)
    if testlock:
        sleep(600)
        ledslock.release()

# pump thread
pumplock = _thread.allocate_lock()
def pump_thread():
    global pumplock,motorontime,motorofftime
    pump = pumplock.acquire(0)
    if pump:
      turn(motor,"on")
      print('turn(motor,"on")'+str(motor.value())+ " " + str(motor.duty()))
      sleep(motorontime)
      turn(motor,"off")
      sleep(motorofftime)
      pumplock.release()

# while loop
while True:
    time = rtc.datetime()
    print(time)
    timevalue = time[4]*60+ time[5]
    if timevalue == 3*60+13:
        set_time()
    if (not ledslock.locked()):
        for i in range(len(schedule["ledsonhour"])):
            leftboundary = schedule["ledsonhour"][i]*60+schedule["ledsonmin"][i]
            rightboundary = schedule["ledsoffhour"][i]*60+schedule["ledsoffmin"][i]
            if (leftboundary<rightboundary):
                if timevalue > leftboundary and timevalue < rightboundary:
                    turn(leds,"on")
                    print('turn(leds,"on1")')
                    break
                else:
                    turn(leds,"off")
                    print('turn(leds,"off")')
            elif (rightboundary<leftboundary):
                if timevalue > rightboundary and timevalue < leftboundary:
                    turn(leds,"on")
                    print('turn(leds,"on")')
                    break
                else:
                    turn(leds,"off")
                    print('turn(leds,"no")')
    for i in range(len(schedule["motoronhour"])):
        leftboundary = schedule["motoronhour"][i]*60+schedule["motoronmin"][i]
        rightboundary = schedule["motoroffhour"][i]*60+schedule["motoroffmin"][i]
        if leftboundary<rightboundary:
            if timevalue > leftboundary and timevalue < rightboundary:
                _thread.start_new_thread(pump_thread, ())
                break
        elif (rightboundary<leftboundary):
            if timevalue > rightboundary and timevalue < leftboundary:
                _thread.start_new_thread(pump_thread, ())
                break
    sleep(2)

